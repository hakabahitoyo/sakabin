#!/bin/bash

if test $REQUEST_METHOD != 'POST'
then
	echo 'Status: 200'
	echo 'Content-Type: text/plain'
	echo ''
	echo 'Invalid method.'
fi

random=$(printf '%04x' $RANDOM)$(printf '%04x' $RANDOM)

cat - > /var/lib/sakabin/images/$random.png

echo 'Status: 200'
echo 'Content-Type: text/plain'
echo ''

