#include <iostream>
#include <filesystem>
#include <vector>
#include <algorithm>
#include <ctime>


using namespace std;
using namespace std::filesystem;


static string escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


static bool by_time_desc (const directory_entry &left, const directory_entry &right) {
	return right.last_write_time () < left.last_write_time ();
}


int main ()
{
	vector <directory_entry> files;

	for (auto file: directory_iterator ("/var/lib/sakabin/images")) {
		if (file.is_regular_file ()) {
			files.push_back (file);
		}
	}

	sort (files.begin (), files.end (), by_time_desc);

	cout << "Content-type: text/plain" << endl;
	cout << endl;

	auto unix_time = time (nullptr);
	unix_time -= 12 * 60 * 60;

	for (unsigned int cn = 0; cn < 1000 && cn < files.size (); cn ++) {
		auto file = files.at (cn);
		string filename = file.path ().filename ().string ();
		cout
		<< "{"
		<< "\"id\":\"" << escape_json (filename) << "\","
		<< "\"time\":\"" << unix_time << "\","
		<< "\"text\":\"" << escape_json (string {"https://ai18n.tk/sakabin-images/"} + filename) << "\""
		<< "}"
		<< endl;
		unix_time -= 24 * 60 * 60;
	}

	return 0;
}
